#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/scs.h>
#include <stdio.h> 
#include "delay.h"
#include "term.h"
#include "util.h"

extern void initialise_monitor_handles(void);
/* Set STM32 to 168 MHz. */
 static void clock_setup(void)
{
    rcc_clock_setup_hse_3v3(&rcc_hse_25mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

    /* Enable GPIOA clock. */
    rcc_periph_clock_enable(RCC_GPIOA);
}

static void gpio_setup(void)
{
    /* Set GPIO8 (in GPIO port A) to 'output push-pull'. */
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT,
                        GPIO_PUPD_NONE, GPIO8);
}

int main(void)
{


    clock_setup();
    gpio_setup();
    ticker_init();
    /* Set two LEDs for wigwag effect when toggling. */
    gpio_set(GPIOA, GPIO8);
    uart_init(); 
    uint16_t l = 0;
    l = delay(1000);
    SCS_DEMCR |= SCS_DEMCR_VC_MON_EN;
    initialise_monitor_handles();
    while(1){
        __asm__("NOP");
    }
    return 0;
}
