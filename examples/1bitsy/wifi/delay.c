#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/cm3/nvic.h>
#include "delay.h"

const uint16_t MAX_TICKS = 10000;
uint8_t tick_index = 0;
uint16_t ticks = 0;
uint16_t ticker[10];
uint8_t ticker_used[10];
/*
 * Initialize systick to one ms 
 * */
void ticker_init(void){
    systick_set_reload(168000);
    systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
    systick_counter_enable();
    /* this done last */
    systick_interrupt_enable();
}

/*
 *  Increments ticks at 1ms, resets all overflows when it reaches 10s 
 * */
void sys_tick_handler(void){
    ticks +=1;
    for (uint8_t i = 0 ; i < 10; i++){
        if (ticker[i] > 0){
            ticker[i] -= 1;
        }
    }
    if (ticks >= MAX_TICKS){
        ticks = 0;
    }
}

/*
 * Adds a delay to ticker; if all slots are taken, returns -1 else 
 * returns a delay/tick index (uses overflow flag incase ticks > ticker) 
 * */
int8_t delay(uint16_t ms){
    if (ms > MAX_TICKS || ms == 0){
        return -1;
    }
    tick_index += 1;
    tick_index %= 10;
    if (ticker[tick_index] == 0){
        ticker[tick_index] = ms;
        ticker_used[tick_index] = 1;
        return tick_index;
    } else {
        return -1;
    }
}

/*
 * returns 1 if delay is elapsed or 0 otherwise (also handles out of bound tick_index and unused tick_indexs  
 * */
uint8_t delay_elapsed(uint8_t index){
    index %= 10;
    if (ticker[index] == 0 && ticker_used[index] == 1 ){
        ticker_used[index] = 0;
        ticker[index] = 0;
        return 1;
    } else {
        return 0;
    }   
}

