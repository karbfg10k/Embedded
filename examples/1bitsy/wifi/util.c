#include "util.h"

/*
 * memset thats optimized to go work 4bytes (32bit) at a time,
 * 
 * */
void* __attribute__ ((weak)) memset(void* s, int c, size_t n){
    uint8_t* buf = s;

    if (n == 0){
        return s;
    }
    buf[0] = buf[n-1] = c;
    if (n <= 2){
        return s;
    }
    buf[1] = buf[n-2] = buf[2] = buf[n-3] = c;
    if (n <= 6){
        return s;
    }
    buf[3] = buf[n-4] = c;
    if (n <= 8){
        return s;
    }

    n -= 8;
    n /= 4;
    uint32_t word_c = c & 0xFF;
    word_c |= (word_c << 24) | (word_c << 16) | (word_c << 8); 
   
    buf += 4;
    uint32_t* word_s = (uint32_t*)buf;
    
    
    for(; n; n-- , word_s++){
        *word_s = word_c;
    }
    *word_s = word_c;

    return s;
}

/*
 * simple strnlen 
 * */
size_t __attribute__ ((weak)) strnlen(const char* str, size_t max_len){
    size_t s = 0;
    while ( str[s++] != '\0' && s <= max_len );
    return (s - 1);
}

/*
 * simple strncmp 
 * */
int __attribute__ ((weak)) strncmp(const char* s1, const char* s2, size_t max_len){
    unsigned char str1 = *s1;
    unsigned char str2 = *s2;
    while(str1 == str2 && str1 != '\0' && max_len > 0){
        str1 = *s1++;
        str2 = *s2++;
        max_len--;
    }

    return str1 - str2;


}

