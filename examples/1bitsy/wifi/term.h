#include <stdlib.h>


/*
 * Used to setup UART4 (terminal) and USART2 for 
 * debug prints into gdb session
 * */
void uart_init(void);

/*
 * Writes out a buffer to UART4 (blocking)
 * */
void uart_write(const char* buf, size_t len);

/*
 * Writes out a buffer to USART2 (blocking)
 * */
void uart_write_2(const char* buf, size_t len);

/*
 * Executes terminal command
 * */
void execute_cmd(void);
