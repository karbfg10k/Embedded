#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>
#include "wifi_control.h"
#include "delay.h"

#define WIFI_SS     GPIO10
#define WIFI_SCK    GPIO13  
#define WIFI_MOSI   GPIO15  
#define WIFI_MISO   GPIO14  
#define WIFI_EN     GPIO3 
#define WIFI_IRQ    GPIO2
#define WIFI_RST    GPIO9
#define WIFI_WAKE   GPIO8

void wifi_spi_init(void){
	gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE,
			(WIFI_SCK | WIFI_MISO | WIFI_MOSI)
			);

	gpio_set_af(GPIOB, GPIO_AF5,
			(WIFI_SCK | WIFI_MISO | WIFI_MOSI)
			);

	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT,
			GPIO_PUPD_NONE, WIFI_SS | WIFI_RST | WIFI_WAKE);

	gpio_mode_setup(GPIOC, GPIO_MODE_OUTPUT,
			GPIO_PUPD_NONE, WIFI_EN);

	gpio_mode_setup(GPIOC, GPIO_MODE_INPUT,
			GPIO_PUPD_NONE, WIFI_IRQ);

	rcc_periph_clock_enable(RCC_SPI2);

	spi_init_master(SPI2, SPI_CR1_BAUDRATE_FPCLK_DIV_2,
			SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, 
			SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);

	spi_disable_crc(SPI2);

	spi_enable_ss_output(SPI2);

	spi_enable(SPI2);
}

void wifi_wake_set(void){
	gpio_set(GPIOB, WIFI_WAKE);
}

void wifi_wake_reset(void){
	gpio_clear(GPIOB, WIFI_WAKE);
}

void wifi_en_set(void){
	gpio_set(GPIOC, WIFI_EN);
}

void wifi_en_reset(void){
	gpio_clear(GPIOC, WIFI_EN);
}

void wifi_reset(void){
	gpio_set(GPIOB, WIFI_RST);
	delay(100); // Not checked
	gpio_clear(GPIOB, WIFI_RST);
}

uint16_t wifi_state(void){
	return gpio_get(GPIOC, WIFI_IRQ); 
} 

uint8_t wifi_send(uint8_t cmd){
    return spi_xfer(SPI2, cmd);
}
