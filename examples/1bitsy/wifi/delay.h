#include <stdint.h>
/*
 * Initialize systick to one ms 
 * */
void ticker_init(void);

/*
 * Adds a delay to ticker; if all slots are taken, returns -1 else 
 * returns a delay/tick index (uses overflow flag incase ticks > ticker) 
 * */
int8_t delay(uint16_t ms);

/*
 * returns 1 if delay is elapsed or 0 otherwise (also handles out of bound tick_index and unused tick_indexs  
 * */
uint8_t delay_elapsed(uint8_t tick_index);

//FOR DEBUGGING
uint16_t get_ticks(void);

