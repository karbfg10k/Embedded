#include <stdint.h>
#include <stdlib.h>

/*
 * memset thats optimized to go work 4bytes (32bit) at a time,
 * 
 * */
void* __attribute__ ((weak)) memset(void* s, int c, size_t n);

/*
 * simple strnlen 
 * */
size_t __attribute__ ((weak)) strnlen(const char* str, size_t max_len);

/*
 * simple strncmp 
 * */
int __attribute__ ((weak)) strncmp(const char* s1, const char* s2, size_t max_len);

