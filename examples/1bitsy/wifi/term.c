#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>
#include "term.h"
#include "util.h"
#include <stdio.h>


#define DEL_CHAR 127
#define RET_CHAR '\r'

#define MAX_CMD_LEN 25
uint8_t cmd[MAX_CMD_LEN];
uint8_t cmd_iter;

/*
 * Used to setup UART4 (terminal) and USART2 for 
 * debug prints into gdb session
 * */
void uart_init(void){

    nvic_enable_irq(NVIC_UART4_IRQ);

    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_UART4);

    /* Setup GPIO pins for UART4 TX & RX mode. */
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO0);
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO1);

    gpio_set_af(GPIOA, GPIO_AF8, GPIO0);
    gpio_set_af(GPIOA, GPIO_AF8, GPIO1);

    /* Setup USART2 parameters. */
    usart_set_baudrate(UART4, 115200);
    usart_set_databits(UART4, 8);
    usart_set_stopbits(UART4, USART_STOPBITS_1);
    usart_set_mode(UART4, USART_MODE_TX_RX);
    usart_set_parity(UART4, USART_PARITY_NONE);
    usart_set_flow_control(UART4, USART_FLOWCONTROL_NONE);

    usart_enable_rx_interrupt(UART4);

    usart_enable(UART4);

    /*Prints to GDB*/
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_USART3);

    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO10);

    gpio_set_af(GPIOB, GPIO_AF7, GPIO10);

    usart_set_baudrate(USART3, 38400);
    usart_set_databits(USART3, 8);
    usart_set_stopbits(USART3, USART_STOPBITS_1);
    usart_set_mode(USART3, USART_MODE_TX);
    usart_set_parity(USART3, USART_PARITY_NONE);
    usart_set_flow_control(USART3, USART_FLOWCONTROL_NONE);

    usart_enable(USART3);
}

/*
 * Writes out a buffer to UART4 (blocking)
 * */
void uart_write(const char* buf, size_t len){
    for (size_t i = 0; i < len; i++){
        usart_send_blocking(UART4, buf[i]);
    }
}

/*
 * Writes out a buffer to USART2 (blocking)
 * */
void uart_write_2(const char* buf, size_t len){
    for (size_t i = 0; i < len; i++){
        usart_send_blocking(USART3, buf[i]);
    }
}

/*
 * UART4 ISR; used to capture terminal commands
 * */
void uart4_isr(void){
    uint8_t c ;
    if(( (USART_CR1(UART4) & USART_CR1_RXNEIE ) != 0 ) &&
            ((USART_SR(UART4) & USART_SR_RXNE) != 0 )) {
        c = usart_recv(UART4);
        if (c == DEL_CHAR){
            if(cmd_iter != 0){
                cmd_iter--;
                cmd[cmd_iter] = '\0';
            }
        } else {
            if (c == RET_CHAR){
                execute_cmd();
            }
            cmd[cmd_iter++] = c;
            if (cmd_iter == MAX_CMD_LEN){
                execute_cmd();
            }
        }
    }
}

/*
 * Executes terminal command
 * */
void execute_cmd(void){
    size_t cmd_len = strnlen((char*)cmd, MAX_CMD_LEN);
    switch(cmd_len){
        default:
            printf("E:%s\n", cmd);
            memset(cmd, 0, MAX_CMD_LEN);
            cmd_iter = 0;
            break;
    }
}

/*
 * All following code is from 1bitsy example for semihostprintf
 * */
enum {
    RDI_SYS_OPEN = 0x01,
    RDI_SYS_WRITE = 0x05,
    RDI_SYS_ISTTY = 0x09,
};

static int rdi_write(int fn, const char *buf, size_t len)
{

    (void)fn;

    uart_write_2(buf, len);

    return (int)len;
}

struct ex_frame {
    union {
        int syscall;
        int retval;
    };
    const int *params;
    uint32_t r2, r3, r12, lr, pc;
};
void debug_monitor_handler_c(struct ex_frame *sp);

void debug_monitor_handler_c(struct ex_frame *sp){
    /* Return to after breakpoint instruction */
    sp->pc += 2;

    switch (sp->syscall) {
        case RDI_SYS_OPEN:
            sp->retval = 1;
            break;
        case RDI_SYS_WRITE:
            sp->retval = rdi_write(sp->params[0], (void*)sp->params[1], sp->params[2]);
            break;
        case RDI_SYS_ISTTY:
            sp->retval = 1;
            break;
        default:
            sp->retval = -1;
    }

}

asm(".globl debug_monitor_handler\n"
        ".thumb_func\n"
        "debug_monitor_handler: \n"
        "    mov r0, sp\n"
        "    b debug_monitor_handler_c\n");
