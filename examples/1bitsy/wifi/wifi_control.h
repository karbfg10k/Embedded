void wifi_spi_init(void);

void wifi_wake_set(void);

void wifi_wake_reset(void);

void wifi_en_set(void);

void wifi_en_reset(void);

void wifi_reset(void);

uint16_t wifi_state(void);

uint8_t wifi_send(uint8_t cmd);

